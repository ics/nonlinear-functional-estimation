# Nonlinear Functional Estimation

This repository contains the code for the numerical example in the paper [Nonlinear Functional Estimation: Functional Detectability and Full Information Estimation](https://arxiv.org/abs/2312.13859). The implementation is based on Casadi and the solver ipopt.
The implementation was tested using Python3.10.12 with packages as included in the requirements.txt file.

## Reference

[Simon Muntwiler, Johannes Köhler, and Melanie N. Zeilinger. Nonlinear Functional Estimation: Functional Detectability and Full Information Estimation. arXiv preprint arXiv:2312.13859, 2024.](https://arxiv.org/abs/2312.13859)

## Abstract

We consider the design of functional estimators, i.e., approaches to compute an estimate of a nonlinear function of the state of a general nonlinear dynamical system subject to process noise based on noisy output measurements. To this end, we introduce a novel functional detectability notion in the form of incremental input/output-to-output stability (δ-IOOS). We show that δ-IOOS is a necessary condition for the existence of a functional estimator satisfying an input-to-output type stability property. Additionally, we prove that a system is functional detectable if and only if it admits a corresponding δ-IOOS Lyapunov function. Furthermore, δ-IOOS is shown to be a sufficient condition for the design of a stable functional estimator by introducing the design of a full information estimation (FIE) approach for functional estimation. Together, we present a unified framework to study functional estimation with a detectability condition, which is necessary and sufficient for the existence of a stable functional estimator, and a corresponding functional estimator design. The practical need for and applicability of the proposed functional estimator design is illustrated with a numerical example of a power system.

# Setup Instructions
* Clone the repository:

        git clone git@gitlab.ethz.ch:ics/nonlinear-functional-estimation.git

* Setup a virtual environment with requirements as stated in requirements.txt:

        python3.10 -m venv env_name
        source env_name/bin/activate
        cd nonlinear-functional-estimation
        pip install -r requirements.txt
        
* Open the ipython notebook:

        jupyter notebook nonlinear-functional-estimation.ipynb

